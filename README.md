# RAK Feedback Screenshot

RAK Feedback - A tool to collect customer feedback in RAK cheramics showroom

## Key Technologies

* Android
* Room persistancy library
* WorkManager for schronization
* Retrofit for networking
* Firebase Crushlytics for crush reporting


### Screen Shots
</br>
<h3>Login page</h3>
<img src="images/page_0.png" height="640" weight="360" />
<hr/>

</br>
<h3>Home page</h3>
<img src="images/page_1.png" height="640" weight="360" />
<hr/>


</br>
<h3>Customer info page</h3>
<img src="images/page_2.png" height="640" weight="360" />
<hr/>


</br>
<h3>Customer Category page</h3>
<img src="images/page_3.png" height="640" weight="360" />
<hr/>


</br>
<h3>Feedback Category page</h3>
<img src="images/page_4.png" height="640" weight="360" />
<hr/>


</br>
<h3>Feedback page</h3>
<img src="images/page_5.png" height="640" weight="360" />

</br></br>
<img src="images/page_6.png" height="640" weight="360" />
<hr/>


</br>
<h3>Thanks page</h3>
<img src="images/page_7.png" height="640" weight="360" />
<hr/>